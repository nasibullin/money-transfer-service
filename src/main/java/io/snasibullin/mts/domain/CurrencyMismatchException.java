package io.snasibullin.mts.domain;

import io.snasibullin.mts.ApplicationException;

public class CurrencyMismatchException extends ApplicationException {

    public CurrencyMismatchException(String message) {
        super(message);
    }

    @Override
    public int httpStatus() {
        return 409;
    }

    @Override
    public String errorCode() {
        return "CurrencyMismatch";
    }

}
