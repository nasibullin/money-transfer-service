package io.snasibullin.mts.domain;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

public class AccountNumber {

    private final String number;

    public AccountNumber(String number) {
        this.number = requireNonNull(number, "AccountNumber");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountNumber that = (AccountNumber) o;
        return number.equals(that.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }

    @Override
    public String toString() {
        return number;
    }

}
