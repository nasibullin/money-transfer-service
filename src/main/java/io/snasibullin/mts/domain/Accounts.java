package io.snasibullin.mts.domain;

import java.util.Optional;

public interface Accounts {

    Iterable<Account> iterate();

    Optional<Account> account(AccountNumber accountNumber);

}
