package io.snasibullin.mts.domain;

import io.snasibullin.mts.ApplicationException;

import static java.lang.String.format;

public class AccountInsufficientFundsException extends ApplicationException {

    public AccountInsufficientFundsException(AccountNumber accountNumber) {
        super(format("Insufficient funds on Account [%s]", accountNumber));
    }

    @Override
    public int httpStatus() {
        return 409;
    }

    @Override
    public String errorCode() {
        return "InsufficientFunds";
    }

}
