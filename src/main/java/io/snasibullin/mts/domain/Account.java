package io.snasibullin.mts.domain;

import org.javamoney.moneta.Money;

import javax.money.CurrencyUnit;

public interface Account {

    AccountNumber number();

    Money balance();

    CurrencyUnit currency();

    void withdraw(Money amount) throws CurrencyMismatchException, AccountInsufficientFundsException;

    void deposit(Money amount) throws CurrencyMismatchException;

    void transfer(Account to, Money amount) throws CurrencyMismatchException, AccountInsufficientFundsException;

}
