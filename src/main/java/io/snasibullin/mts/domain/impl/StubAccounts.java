package io.snasibullin.mts.domain.impl;

import io.snasibullin.mts.domain.Account;
import io.snasibullin.mts.domain.AccountNumber;
import io.snasibullin.mts.domain.Accounts;

import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static java.util.Optional.ofNullable;

public class StubAccounts implements Accounts {

    private final ConcurrentMap<AccountNumber, Account> accounts = new ConcurrentHashMap<>();

    public StubAccounts(Collection<Account> accounts) {
        accounts.stream().forEach(account -> this.accounts.put(account.number(), account));
    }

    @Override
    public Iterable<Account> iterate() {
        return accounts.values();
    }

    @Override
    public Optional<Account> account(AccountNumber accountNumber) {
        return ofNullable(accounts.get(accountNumber));
    }

}
