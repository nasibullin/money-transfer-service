package io.snasibullin.mts.domain.impl;

import io.snasibullin.mts.domain.Account;
import io.snasibullin.mts.domain.AccountInsufficientFundsException;
import io.snasibullin.mts.domain.AccountNumber;
import io.snasibullin.mts.domain.CurrencyMismatchException;
import org.javamoney.moneta.Money;

import javax.money.CurrencyUnit;
import java.util.concurrent.atomic.AtomicReference;

import static java.lang.String.format;

public class StubAccount implements Account {

    private final AccountNumber accountNumber;
    private final AtomicReference<Money> balance;

    public StubAccount(AccountNumber accountNumber, Money balance) {
        this.accountNumber = accountNumber;
        this.balance = new AtomicReference<>(balance);
    }

    @Override
    public AccountNumber number() {
        return accountNumber;
    }

    @Override
    public Money balance() {
        return balance.get();
    }

    @Override
    public CurrencyUnit currency() {
        return balance.get().getCurrency();
    }

    @Override
    public void withdraw(Money amount) throws CurrencyMismatchException, AccountInsufficientFundsException {
        if (!currency().equals(amount.getCurrency())) {
            throw new CurrencyMismatchException(format("Withdraw [%s] from [%s] Account is unsupported", amount.getCurrency(), currency()));
        }
        balance.updateAndGet(currentBalance -> {
            if (amount.isGreaterThan(currentBalance)) {
                throw new AccountInsufficientFundsException(accountNumber);
            }
            return currentBalance.subtract(amount);
        });
    }

    @Override
    public void deposit(Money amount) {
        if (!currency().equals(amount.getCurrency())) {
            throw new CurrencyMismatchException(format("Deposit [%s] on [%s] Account is unsupported", amount.getCurrency(), currency()));
        }
        balance.updateAndGet(currentBalance -> currentBalance.add(amount));
    }

    @Override
    public void transfer(Account to, Money amount) throws AccountInsufficientFundsException {
        if (!currency().equals(to.currency())) {
            throw new CurrencyMismatchException(format("MoneyTransfer between [%s] and [%s] Accounts is unsupported", currency(), to.currency()));
        }
        withdraw(amount);
        to.deposit(amount);
    }

}
