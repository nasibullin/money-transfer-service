package io.snasibullin.mts.domain.impl;

import io.snasibullin.mts.domain.AccountInsufficientFundsException;
import io.snasibullin.mts.domain.AccountNumber;
import org.javamoney.moneta.Money;
import org.openjdk.jcstress.annotations.*;
import org.openjdk.jcstress.infra.results.LLL_Result;

import static java.math.BigDecimal.valueOf;
import static org.javamoney.moneta.Money.of;

@JCStressTest
@State
@Outcome(id = "70, 30, 250", expect = Expect.ACCEPTABLE, desc = "Actor2 and Actor3, Actor1 failed")
@Outcome(id = "20, 280, 50", expect = Expect.ACCEPTABLE, desc = "Actor2 and Actor1, Actor3 failed")
@Outcome(expect = Expect.FORBIDDEN)
public class StubAccountConcurrencyTest {

    StubAccount acc1 = new StubAccount(new AccountNumber("1"), of(200, "RUB"));
    StubAccount acc2 = new StubAccount(new AccountNumber("2"), of(100, "RUB"));
    StubAccount acc3 = new StubAccount(new AccountNumber("2"), of(50, "RUB"));

    @Actor
    public void actor1() {
        try {
            acc1.transfer(acc2, Money.of(valueOf(250), "RUB"));
        } catch (AccountInsufficientFundsException e) {

        }
    }

    @Actor
    public void actor2() {
        acc2.transfer(acc1, Money.of(valueOf(70), "RUB"));
    }

    @Actor
    public void actor3() {
        try {
            acc1.transfer(acc3, Money.of(valueOf(200), "RUB"));
        } catch (AccountInsufficientFundsException e) {

        }
    }

    @Arbiter
    public void arbiter(LLL_Result result) {
        result.r1 = acc1.balance().getNumber().longValue();
        result.r2 = acc2.balance().getNumber().longValue();
        result.r3 = acc3.balance().getNumber().longValue();
    }

}
