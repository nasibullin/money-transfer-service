package io.snasibullin.mts;

public abstract class ApplicationException extends RuntimeException {

    public ApplicationException(String message) {
        super(message);
    }

    public ApplicationException(Throwable cause) {
        super(cause);
    }

    public abstract int httpStatus();

    public abstract String errorCode();

}
