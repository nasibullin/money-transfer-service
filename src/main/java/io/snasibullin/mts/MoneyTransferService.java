package io.snasibullin.mts;

import io.snasibullin.mts.api.AccountCollectionResource;
import io.snasibullin.mts.api.ApplicationExceptionMapper;
import io.snasibullin.mts.api.MoneyTransferResource;
import io.snasibullin.mts.api.OtherExceptionMapper;
import io.snasibullin.mts.domain.AccountNumber;
import io.snasibullin.mts.domain.Accounts;
import io.snasibullin.mts.domain.impl.StubAccount;
import io.snasibullin.mts.domain.impl.StubAccounts;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.server.ResourceConfig;
import org.javamoney.moneta.Money;

import java.math.BigDecimal;
import java.net.URI;

import static java.util.Arrays.asList;
import static org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory.createHttpServer;

public class MoneyTransferService {

    static final String BASE_URI = "http://localhost:8080/";

    static HttpServer startServer(Accounts accounts) {
        final ResourceConfig resourceConfig = new ResourceConfig().
                register(new AccountCollectionResource(accounts)).
                register(new MoneyTransferResource(accounts)).
                register(new ApplicationExceptionMapper()).
                register(new OtherExceptionMapper());
        return createHttpServer(URI.create(BASE_URI), resourceConfig);
    }

    public static void main(String[] args) {
        final HttpServer server = startServer(accounts());
        Runtime.getRuntime().addShutdownHook(new Thread(server::shutdownNow));
    }

    private static StubAccounts accounts() {
        return new StubAccounts(asList(
                new StubAccount(new AccountNumber("rub-1"), Money.of(BigDecimal.valueOf(100), "RUB")),
                new StubAccount(new AccountNumber("rub-2"), Money.of(BigDecimal.valueOf(100), "RUB")),
                new StubAccount(new AccountNumber("usd-1"), Money.of(BigDecimal.valueOf(100), "USD"))
        ));
    }

}

