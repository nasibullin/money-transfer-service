package io.snasibullin.mts.api;

import io.snasibullin.mts.ApplicationException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class ApplicationExceptionMapper implements ExceptionMapper<ApplicationException> {

    @Override
    public Response toResponse(ApplicationException exception) {
        return Response.
                status(exception.httpStatus()).
                entity(new Error(exception.errorCode(), exception.getMessage())).
                type(MediaType.APPLICATION_JSON).
                build();
    }

}
