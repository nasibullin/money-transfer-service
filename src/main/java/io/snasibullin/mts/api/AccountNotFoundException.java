package io.snasibullin.mts.api;

import io.snasibullin.mts.ApplicationException;
import io.snasibullin.mts.domain.AccountNumber;

import static java.lang.String.format;

public class AccountNotFoundException extends ApplicationException {

    private final int httpStatusCode;
    private final String code;

    public AccountNotFoundException(AccountNumber accountNumber, int httpStatusCode, String code) {
        super(format("Account [%s] not found", accountNumber));
        this.httpStatusCode = httpStatusCode;
        this.code = code;
    }

    @Override
    public int httpStatus() {
        return httpStatusCode;
    }

    @Override
    public String errorCode() {
        return code;
    }

}
