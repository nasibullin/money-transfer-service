package io.snasibullin.mts.api;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class OtherExceptionMapper implements ExceptionMapper<Exception> {

    @Override
    public Response toResponse(Exception exception) {
        return Response.
                status(500).
                entity(new Error("InternalServerError", exception.getMessage())).
                type(MediaType.APPLICATION_JSON).
                build();
    }

}
