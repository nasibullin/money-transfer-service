package io.snasibullin.mts.api;

import io.snasibullin.mts.domain.Account;
import io.snasibullin.mts.domain.Accounts;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

@Path("money-transfers")
public class MoneyTransferResource {

    private final Accounts accounts;

    public MoneyTransferResource(Accounts accounts) {
        this.accounts = accounts;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void transfer(@Valid TransferRequest transferRequest) {
        Account from = accounts.account(transferRequest.from()).orElseThrow(() -> new AccountNotFoundException(transferRequest.from(), 400, "InvalidFrom"));
        Account to = accounts.account(transferRequest.to()).orElseThrow(() -> new AccountNotFoundException(transferRequest.to(), 400, "InvalidTo"));
        from.transfer(to, transferRequest.amount());
    }

}
