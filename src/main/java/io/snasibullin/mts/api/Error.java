package io.snasibullin.mts.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Error {

    private final String code;
    private final String message;

    public Error(String code, String message) {
        this.code = code;
        this.message = message;
    }

    @JsonProperty("code")
    public String code() {
        return code;
    }

    @JsonProperty("message")
    public String message() {
        return message;
    }
}
