package io.snasibullin.mts.api;

import io.snasibullin.mts.ApplicationException;

public class InvalidRequestBodyException extends ApplicationException {

    private final String code;

    public InvalidRequestBodyException(String code, Throwable cause) {
        super(cause);
        this.code = code;
    }

    public InvalidRequestBodyException(String code, String message) {
        super(message);
        this.code = code;
    }

    @Override
    public int httpStatus() {
        return 400;
    }

    @Override
    public String errorCode() {
        return code;
    }

}
