package io.snasibullin.mts.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.snasibullin.mts.domain.AccountNumber;
import org.javamoney.moneta.Money;

import javax.money.MonetaryException;

import static java.util.Optional.ofNullable;

public class TransferRequest {

    private final String from;
    private final String to;
    private final String amount;

    @JsonCreator
    public TransferRequest(
            @JsonProperty("from") String from,
            @JsonProperty("to") String to,
            @JsonProperty("amount") String amount) {
        this.from = from;
        this.to = to;
        this.amount = amount;
    }

    public AccountNumber from() {
        return ofNullable(from).map(AccountNumber::new).orElseThrow(() -> new InvalidRequestBodyException("InvalidFrom", "TransferRequest.from is mandatory"));
    }

    public AccountNumber to() {
        return ofNullable(to).map(AccountNumber::new).orElseThrow(() -> new InvalidRequestBodyException("InvalidTo", "TransferRequest.to is mandatory"));
    }

    public Money amount() {
        try {
            return ofNullable(amount).map(MoneyAsString::new).map(MoneyAsString::toMoney).orElseThrow(() -> new InvalidRequestBodyException("InvalidAmount", "TransferRequest.amount is mandatory"));
        } catch (MonetaryException e) {
            throw new InvalidRequestBodyException("InvalidAmount", e);
        }
    }

}
