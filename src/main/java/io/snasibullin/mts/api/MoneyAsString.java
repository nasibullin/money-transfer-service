package io.snasibullin.mts.api;

import org.javamoney.moneta.Money;

import java.util.Locale;

import static javax.money.format.MonetaryFormats.getAmountFormat;

public class MoneyAsString {

    private final Money money;

    public MoneyAsString(Money money) {
        this.money = money;
    }

    public MoneyAsString(String string) {
        this(Money.parse(string, getAmountFormat(Locale.ENGLISH)));
    }

    @Override
    public String toString() {
        return getAmountFormat(Locale.ENGLISH).format(money);
    }

    public Money toMoney() {
        return money;
    }

}
