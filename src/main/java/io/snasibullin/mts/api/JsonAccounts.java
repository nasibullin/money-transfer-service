package io.snasibullin.mts.api;

import io.snasibullin.mts.domain.Account;

import java.util.Iterator;

import static com.google.common.collect.Streams.stream;

public class JsonAccounts implements Iterable<JsonAccount> {

    private final Iterable<Account> accounts;

    public JsonAccounts(Iterable<Account> accounts) {
        this.accounts = accounts;
    }

    @Override
    public Iterator<JsonAccount> iterator() {
        return stream(accounts).map(JsonAccount::new).iterator();
    }

}
