package io.snasibullin.mts.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.snasibullin.mts.domain.Account;

public class JsonAccount {

    private final Account account;

    public JsonAccount(Account account) {
        this.account = account;
    }

    @JsonProperty("number")
    public String number() {
        return account.number().toString();
    }

    @JsonProperty("balance")
    public String balance() {
        return new MoneyAsString(account.balance()).toString();
    }

}
