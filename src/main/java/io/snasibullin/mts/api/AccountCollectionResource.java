package io.snasibullin.mts.api;

import io.snasibullin.mts.domain.Accounts;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("accounts")
public class AccountCollectionResource {

    private final Accounts accounts;

    public AccountCollectionResource(Accounts accounts) {
        this.accounts = accounts;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public JsonAccounts list() {
        return new JsonAccounts(accounts.iterate());
    }

}
