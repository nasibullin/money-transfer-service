package io.snasibullin.mts;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import com.google.common.io.CharStreams;
import io.snasibullin.mts.domain.AccountNumber;
import io.snasibullin.mts.domain.impl.StubAccount;
import io.snasibullin.mts.domain.impl.StubAccounts;
import org.glassfish.grizzly.http.server.HttpServer;
import org.javamoney.moneta.Money;
import org.json.JSONException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.stream.Stream;

import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasJsonPath;
import static java.lang.String.format;
import static java.util.Arrays.asList;
import static javax.ws.rs.client.Entity.entity;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class MoneyTransferServiceTest {

    private HttpServer server;
    private WebTarget target;

    @BeforeEach
    void setUp() {
        server = MoneyTransferService.startServer(new StubAccounts(asList(
                new StubAccount(new AccountNumber("rub-1"), Money.of(BigDecimal.valueOf(100), "RUB")),
                new StubAccount(new AccountNumber("rub-2"), Money.of(BigDecimal.valueOf(100), "RUB")),
                new StubAccount(new AccountNumber("usd-1"), Money.of(BigDecimal.valueOf(100), "USD"))
        )));
        target = ClientBuilder.newClient().target(MoneyTransferService.BASE_URI);
    }

    @AfterEach
    void tearDown() {
        server.shutdownNow();
    }

    @Test
    @DisplayName("GET /accounts return HTTP 200 with all accounts")
    void testAccountList() throws IOException, JSONException {
        Response response = target.path("accounts").request().get();

        assertEquals(200, response.getStatus());
        assertEquals(APPLICATION_JSON_TYPE, response.getMediaType());
        JSONAssert.assertEquals(
                textOf("account-list.json"),
                response.readEntity(String.class),
                JSONCompareMode.NON_EXTENSIBLE
        );
    }

    @Test
    @DisplayName("POST /money-transfers return HTTP 204 when money transfer completed")
    void testMoneyTransfer() throws IOException {
        Response response = target.path("money-transfers").request().post(entity(textOf("money-transfer.json"), APPLICATION_JSON_TYPE));

        assertEquals(204, response.getStatus());
        assertFalse(response.hasEntity());

        assertThatAccountHasBalance("rub-1", "RUB50.00");
        assertThatAccountHasBalance("rub-2", "RUB150.00");
    }

    @Test
    @DisplayName("POST /money-transfers return HTTP 409 with 'InsufficientFunds' error code when insufficient funds on from account")
    void testInsufficientFundsOnMoneyTransfer() throws IOException {
        Response response = target.path("money-transfers").request().post(entity(textOf("insufficient-funds-money-transfer.json"), APPLICATION_JSON_TYPE));

        assertEquals(409, response.getStatus());

        String error = response.readEntity(String.class);
        assertThat(error, hasJsonPath("$.code", equalTo("InsufficientFunds")));
        assertThatAccountHasBalance("rub-1", "RUB100.00");
        assertThatAccountHasBalance("rub-2", "RUB100.00");
    }

    @DisplayName("POST /money-transfers return HTTP 409 with 'CurrencyMismatch' when transfer:")
    @ValueSource(strings = {
            "transfer-usd-between-rub-accounts.json",
            "transfer-rub-from-rub-account-to-usd-account-json",
            "transfer-rub-from-usd-account-to-rub-account-json"
    })
    @ParameterizedTest
    void testCurrencyMismatchOnMoneyTransfer(String fileWithRequest) throws IOException {
        Response response = target.path("money-transfers").request().post(entity(textOf(fileWithRequest), APPLICATION_JSON_TYPE));

        assertEquals(409, response.getStatus());

        String error = response.readEntity(String.class);
        assertThat(error, hasJsonPath("$.code", equalTo("CurrencyMismatch")));
        assertThatAccountHasBalance("rub-1", "RUB100.00");
        assertThatAccountHasBalance("rub-2", "RUB100.00");
        assertThatAccountHasBalance("usd-1", "USD100.00");
    }

    private static Stream<Arguments> http400OnMoneyTransfer() {
        return Stream.of(
                Arguments.of("without-from-money-transfer.json", "InvalidFrom"),
                Arguments.of("unknown-from-money-transfer.json", "InvalidFrom"),
                Arguments.of("without-to-money-transfer.json", "InvalidTo"),
                Arguments.of("unknown-to-money-transfer.json", "InvalidTo"),
                Arguments.of("invalid-amount-money-transfer.json", "InvalidAmount"),
                Arguments.of("without-amount-money-transfer.json", "InvalidAmount")
        );
    }
    @DisplayName("POST /money-transfers return HTTP 400 when request is invalid:")
    @ParameterizedTest
    @MethodSource("http400OnMoneyTransfer")
    void testMoneyTransferWithInvalidRequest(String fileWithRequest, String errorCode) throws IOException {
        Response response = target.path("money-transfers").request().post(entity(textOf(fileWithRequest), APPLICATION_JSON_TYPE));

        assertEquals(400, response.getStatus());
        String error = response.readEntity(String.class);
        assertThat(error, hasJsonPath("$.code", equalTo(errorCode)));
        assertThatAccountHasBalance("rub-1", "RUB100.00");
        assertThatAccountHasBalance("rub-2", "RUB100.00");
        assertThatAccountHasBalance("usd-1", "USD100.00");
    }

    private String textOf(String filename) throws IOException {
        return CharStreams.toString(new FileReader("src/test/resources/" + filename));
    }

    private void assertThatAccountHasBalance(String accountNumber, String balance) {
        String accounts = target.path("accounts").request().get().readEntity(String.class);
        assertThat(accounts, hasJsonPath(format("$[?(@.number == '%s')].balance", accountNumber), contains(balance)));
    }

}

